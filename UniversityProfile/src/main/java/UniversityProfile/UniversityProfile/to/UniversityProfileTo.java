package UniversityProfile.UniversityProfile.to;

import java.util.List;

public class UniversityProfileTo {
	private String universityName;
	private String foundedYear;
	private String address;
	private String about;
	private String contactNumber;
	private String universityLink;
	private List<String> coursesOffered;
	
	/**
	 * @return the universityName
	 */
	public String getUniversityName() {
		return universityName;
	}
	/**
	 * @param universityName the universityName to set
	 */
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	/**
	 * @return the foundedYear
	 */
	public String getFoundedYear() {
		return foundedYear;
	}
	/**
	 * @param foundedYear the foundedYear to set
	 */
	public void setFoundedYear(String foundedYear) {
		this.foundedYear = foundedYear;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the about
	 */
	public String getAbout() {
		return about;
	}
	/**
	 * @param about the about to set
	 */
	public void setAbout(String about) {
		this.about = about;
	}
	/**
	 * @return the coursesOffered
	 */
	public List<String> getCoursesOffered() {
		return coursesOffered;
	}
	/**
	 * @param coursesOffered the coursesOffered to set
	 */
	public void setCoursesOffered(List<String> coursesOffered) {
		this.coursesOffered = coursesOffered;
	}
	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}
	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	/**
	 * @return the universityLink
	 */
	public String getUniversityLink() {
		return universityLink;
	}
	/**
	 * @param universityLink the universityLink to set
	 */
	public void setUniversityLink(String universityLink) {
		this.universityLink = universityLink;
	}
	
}
